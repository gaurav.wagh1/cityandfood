# City and Food
Show the list of most popular food dishes and main cities in Japan

## Screenshots

![image 1](./screenshots/1.png) 
![image 2](./screenshots/2.png)

## Sample data
``
{
  "foods": [
    {
      "name": "Sushi",
      "image": "https://robata-kaba.jp/wp-content/uploads/2020/02/susi.jpg"
    },
    {
      "name": "Ramen",
      "image": "https://glebekitchen.com/wp-content/uploads/2017/04/tonkotsuramenfront.jpg"
    }
  ],
  "cities": [
    {
      "name": "Tokyo",
      "image": "https://rimage.gnst.jp/livejapan.com/public/article/detail/a/00/01/a0001379/img/basic/a0001379_main.jpg",
      "description": "Tokyo officially Tokyo Metropolis (東京都, Tōkyō-to), is the de facto capital and most populous prefecture of Japan"
    },
    {
      "name": "Nagoya",
      "image": "https://gaijinpot.scdn3.secure.raxcdn.com/app/uploads/sites/6/2019/02/nagoya-city-1024x684.jpg",
      "description": "Nagoya (名古屋市, Nagoya-shi) is the largest city in the Chūbu region of Japan"
    }
  ]
}
``

## 3rd party libraries
1. [Groupie](https://github.com/lisawray/groupie ) - To render items on recycle view
  
2. [Glide](https://github.com/bumptech/glide) - To load image with URL

3. [okhttp](https://github.com/square/okhttp) - To make http calls


