package com.gaara.citiesandfood.adapter.item

import android.content.Context
import com.bumptech.glide.Glide
import com.gaara.citiesandfood.R
import com.gaara.citiesandfood.data.City
import com.gaara.citiesandfood.databinding.ItemCityBinding
import com.xwray.groupie.databinding.BindableItem

class CityItem(private val context: Context, private val city: City, private val onClickListener: () -> Unit) : BindableItem<ItemCityBinding>() {

    override fun getLayout(): Int {
        return R.layout.item_city
    }

    override fun bind(viewBinding: ItemCityBinding, position: Int) {
        viewBinding.city = city
        viewBinding.cityCardView.setOnClickListener {
            onClickListener()
        }
        Glide.with(context)
            .load(city.image)
            .placeholder(R.drawable.ic_launcher_background)
            .into(viewBinding.cityThumbnailView);
    }
}
