package com.gaara.citiesandfood.adapter.item

import com.gaara.citiesandfood.R
import com.gaara.citiesandfood.databinding.ItemHeaderBinding
import com.xwray.groupie.databinding.BindableItem

class HeaderItem(private val title: String) : BindableItem<ItemHeaderBinding>() {

    override fun getLayout(): Int {
        return R.layout.item_header
    }

    override fun bind(viewBinding: ItemHeaderBinding, position: Int) {
        viewBinding.headerView.text = title
    }
}
