package com.gaara.citiesandfood.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gaara.citiesandfood.api.API

class CityFoodViewModelFactory constructor(private val api: API) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(CityFoodViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            CityFoodViewModel(this.api) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}
