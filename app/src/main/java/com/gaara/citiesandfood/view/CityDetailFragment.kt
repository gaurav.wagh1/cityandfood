package com.gaara.citiesandfood.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.gaara.citiesandfood.R
import com.gaara.citiesandfood.databinding.FragmentCityDetailBinding
import com.gaara.citiesandfood.viewmodel.CityDetailViewModel
import kotlinx.android.synthetic.main.fragment_city_detail.imageView

class CityDetailFragment : Fragment() {

    private val cityDetailViewModel by viewModels<CityDetailViewModel>()
    private lateinit var binding: FragmentCityDetailBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentCityDetailBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = cityDetailViewModel
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            binding.city = cityDetailViewModel.getCity(
                CityDetailFragmentArgs.fromBundle(it).name,
                CityDetailFragmentArgs.fromBundle(it).description,
                CityDetailFragmentArgs.fromBundle(it).image
            )

            loadImage(CityDetailFragmentArgs.fromBundle(it).image)
        }
    }

    private fun loadImage(url: String) {
        Glide.with(this)
            .load(url)
            .placeholder(R.drawable.ic_launcher_background)
            .into(imageView);
    }

}
