package com.gaara.citiesandfood

import com.gaara.citiesandfood.api.API
import com.gaara.citiesandfood.viewmodel.CityFoodViewModelFactory
import okhttp3.OkHttpClient

object Module {
    private val okHttpClient: OkHttpClient by lazy { OkHttpClient() }

    private val api: API by lazy { API(okHttpClient) }

    val cityFoodViewModelFactory: CityFoodViewModelFactory by lazy {
        CityFoodViewModelFactory(api = api)
    }
}