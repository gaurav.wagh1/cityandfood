package com.gaara.citiesandfood.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gaara.citiesandfood.data.CityFood
import com.gaara.citiesandfood.data.Result
import com.gaara.citiesandfood.data.asCityList
import com.gaara.citiesandfood.data.asFoodList

import java.io.IOException
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject

/**
 * This class executes data fetching tasks
 */
class API(private val okHttpClient: OkHttpClient) {

    private val dataUrl = "https://api.npoint.io/a2b63ef226c08553b2f9"
    private val data = MutableLiveData<Result<CityFood>>()

    /**
     * Get list of city and food data
     *
     * @return Live data of city and food list
     */
    fun getCitiesAndFood(): LiveData<Result<CityFood>> {

        val request: Request = Request.Builder()
            .url(dataUrl)
            .build()

        data.value = Result.loading(null)

        okHttpClient.newCall(request)
            .enqueue(object : Callback {
                override fun onResponse(call: Call, response: Response) {
                    if (response.isSuccessful) {
                        try {
                            val responseObject = JSONObject(response.body?.string())
                            data.postValue(
                                Result.success(
                                    CityFood(
                                        cities = responseObject.getJSONArray("cities").asCityList(),
                                        foods = responseObject.getJSONArray("foods").asFoodList()
                                    )
                                )
                            )
                        } catch (json: JSONException) {
                            data.postValue(Result.error("", null))
                        }
                    } else {
                        data.postValue(Result.error("", null))
                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    data.postValue(Result.error("", null))
                }
            })

        return data
    }
}
