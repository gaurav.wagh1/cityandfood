package com.gaara.citiesandfood.viewmodel

import androidx.lifecycle.ViewModel
import com.gaara.citiesandfood.data.City

class CityDetailViewModel : ViewModel() {

    fun getCity(name: String, description: String, image: String): City {
        return City(name = name, description = description, image = image)
    }

}
