package com.gaara.citiesandfood.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.gaara.citiesandfood.api.API
import com.gaara.citiesandfood.data.CityFood
import com.gaara.citiesandfood.data.Result

class CityFoodViewModel(val api: API) : ViewModel() {

    val data: LiveData<Result<CityFood>> = api.getCitiesAndFood()

    fun getCitiesAndFood() {
         api.getCitiesAndFood()
    }
}
