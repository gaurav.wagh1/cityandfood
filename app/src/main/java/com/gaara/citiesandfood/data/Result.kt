package com.gaara.citiesandfood.data

/**
 * A class that holds a status values.
 * @param <T>
</T> */
data class Result<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Result<T> {
            return Result(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Result<T> {
            return Result(Status.ERROR, data, msg)
        }

        fun <T> loading(data: T?): Result<T> {
            return Result(Status.LOADING, data, null)
        }
    }

    /**
     * Status of a result that is provided to the UI.
     *
     *
     * These are usually created by the Repository classes where they return
     * `LiveData<Result<T>>` to pass back the latest data to the UI with its fetch status.
     */
    enum class Status {
        SUCCESS,
        ERROR,
        LOADING,
    }
}

