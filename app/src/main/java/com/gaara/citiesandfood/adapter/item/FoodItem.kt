package com.gaara.citiesandfood.adapter.item

import android.content.Context
import com.bumptech.glide.Glide
import com.gaara.citiesandfood.R
import com.gaara.citiesandfood.data.Food
import com.gaara.citiesandfood.databinding.ItemFoodBinding
import com.xwray.groupie.databinding.BindableItem

class FoodItem(private val context: Context, private val food: Food) : BindableItem<ItemFoodBinding>() {

    override fun getLayout(): Int {
        return R.layout.item_food
    }

    override fun bind(viewBinding: ItemFoodBinding, position: Int) {
        viewBinding.food = food
        Glide.with(context).load(food.image).into(viewBinding.foodThumbnailView)
    }
}
