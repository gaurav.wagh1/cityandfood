package com.gaara.citiesandfood.data

import org.json.JSONArray
import org.json.JSONObject

data class CityFood(
    val cities: List<City>,
    val foods: List<Food>
)

data class City (
    val name: String,
    val image: String,
    val description: String
)

data class Food(
    val name: String,
    val image: String
)

fun JSONArray.asCityList(): List<City> {
    val list = ArrayList<City>()
    for (i in 0 until this.length()) {
        val cityObject: JSONObject = this.getJSONObject(i)
        list.add(
            City(
                cityObject.getString("name"),
                cityObject.getString("image"),
                cityObject.getString("description")
            )
        )
    }
    return list
}

fun JSONArray.asFoodList(): List<Food> {
    val list = ArrayList<Food>()
    for (i in 0 until this.length()) {
        val foodObject: JSONObject = this.getJSONObject(i)
        list.add(
            Food(
                foodObject.getString("name"),
                foodObject.getString("image")
            )
        )
    }
    return list
}
