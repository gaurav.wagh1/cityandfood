package com.gaara.citiesandfood.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.gaara.citiesandfood.Module
import com.gaara.citiesandfood.R
import com.gaara.citiesandfood.adapter.item.CityItem
import com.gaara.citiesandfood.adapter.item.FoodItem
import com.gaara.citiesandfood.adapter.item.HeaderItem
import com.gaara.citiesandfood.data.Result
import com.gaara.citiesandfood.databinding.FragmentCityAndFoodBinding
import com.gaara.citiesandfood.viewmodel.CityFoodViewModel
import com.xwray.groupie.GroupieAdapter
import com.xwray.groupie.groupiex.plusAssign
import kotlinx.android.synthetic.main.fragment_city_and_food.*

class CityFoodFragment : Fragment() {

    private val cityFoodViewModel by viewModels<CityFoodViewModel> { Module.cityFoodViewModelFactory }
    private lateinit var binding: FragmentCityAndFoodBinding
    private val groupAdapter = GroupieAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCityAndFoodBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = cityFoodViewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerview.adapter = groupAdapter

        // SetOnRefreshListener on SwipeRefreshLayout
        swipeRefreshLayout.setOnRefreshListener {
            cityFoodViewModel.getCitiesAndFood()
        }

        swipeRefreshLayout.isRefreshing = true

        cityFoodViewModel.data.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            when {
                it.status === Result.Status.SUCCESS -> {
                    swipeRefreshLayout.isRefreshing = false
                    groupAdapter.clear()

                    // Populate list with data
                    groupAdapter += HeaderItem(resources.getString(R.string.city_title))
                    it.data?.cities?.forEach { city ->

                        groupAdapter += CityItem(context = this.requireContext(), city = city) {
                            findNavController().navigate(
                                CityFoodFragmentDirections.toCityDetails(
                                    name = city.name,
                                    image = city.image,
                                    description = city.description
                                )
                            )

                        }
                    }
                    groupAdapter += HeaderItem(resources.getString(R.string.food_title))
                    it.data?.foods?.forEach {
                        groupAdapter += FoodItem(context = this.requireContext(), food = it)
                    }

                }
                it.status === Result.Status.LOADING -> {
                    swipeRefreshLayout.isRefreshing = true
                }
                it.status === Result.Status.ERROR -> {
                    groupAdapter.clear()
                    swipeRefreshLayout.isRefreshing = false
                }
            }
        })

    }

}
